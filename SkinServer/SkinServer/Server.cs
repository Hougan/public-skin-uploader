﻿using System;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Sockets;
using SkinServer;
using Convert=System.Convert;
using MemoryStream=System.IO.MemoryStream;
using Image=System.Drawing.Image;
 
namespace SocketTcpServer
{
    class Server
    {
        #region Variables

        private static string Ip = "0.0.0.0";
        private static int Port = 8005; 

        #endregion
        static void Main(string[] args)
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(Ip), Port);
            Socket listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            
            try
            {
                listenSocket.Bind(ipPoint);
                listenSocket.Listen(10);
 
                Console.WriteLine("Сервер запущен. Ожидание подключений...");
 
                while (true)
                {
                    Socket handler = listenSocket.Accept();
                    
                    Client client = new Client(handler);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Main(args);
            }
        }

        public static bool SaveImage(string base64)
        {
            try
            {
                byte[] data = Convert.FromBase64String(base64);

                using (var stream = new MemoryStream(data, 0, data.Length))
                {
                    Image image = Image.FromStream(stream);
                    image.Save(
                        @"C:\Users\Hougan\Documents\Projects\SkinServer\SkinServer\bin\Debug\netcoreapp3.1\MyImage.png");
                }
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}