﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Facepunch;
using Steamworks;

namespace SkinServer
{
    public class Client
    {
        private System.Drawing.Image initialImage;

        public static class RustSDK
        {
            public static Steamworks.AppId_t sdkGame = new Steamworks.AppId_t(252490);
            public static Steamworks.AppId_t sdkApp = new Steamworks.AppId_t(391750);
        }
        
        private Socket Socket;
        
        public Client(Socket socket)
        {
            this.Socket = socket;
            ReadData();
        }

        private void ReadData()
        {
            StringBuilder builder = new StringBuilder();
            
            int bytes = 0;
            byte[] data = new byte[10240 * 10240];

            do
            {
                bytes = Socket.Receive(data);
                builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
                Thread.Sleep(100);
            } while (Socket.Available > 0);
                
            ParseData(builder.ToString());
            
            Socket.Shutdown(SocketShutdown.Both);
            Socket.Close();
        }

        private void ParseData(string base64)
        {
            if (base64 == "check")
            {
                Socket.Send(Encoding.UTF8.GetBytes("ok"));
                return;
            }
            
            var images = base64.Split('~');
            Console.WriteLine(images[0]);
            
            Console.WriteLine($"{DateTime.Now.ToShortTimeString()} Получено: {images.Length} изображение (-ий)!");
            foreach (var image in images)
            {
                string message = "";
                byte[] data = null;
                
                try
                {
                    SaveImage(image);
                }
                catch
                {
                    Console.WriteLine($"{false}~Неверное изображение");
                    message = $"{false}~Неверное изображение";
                    data = Encoding.UTF8.GetBytes(message);
                    Socket.Send(data);
                    return;
                }
                
                var result = LoadImage(image);
                
                message = $"{result.Item1}~{result.Item2}";
                data = Encoding.UTF8.GetBytes(message);
                Socket.Send(data);
                
                Console.WriteLine($"{result.Item1}: {result.Item2}");
            }
        }

        private void SaveImage(string base64)
        {
            if (base64.StartsWith("data:image/png;base64,"))
            {
                base64 = base64.Replace("data:image/png;base64,", "");
            }
            Console.WriteLine(base64.Substring(0, 100));
            byte[] imgBytes = Convert.FromBase64String(base64);

            using (var imageFile = new FileStream(@"./data/icon.png", FileMode.Create))
            {
                imageFile.Write(imgBytes, 0, imgBytes.Length);
                imageFile.Flush();
            }
        }
        
        private Tuple<int, string> LoadImage(string base64)
        {
            string id = "";
            SteamAPICall_t pHandle;
            
            try
            {
                SteamAPI.Init();
                pHandle = SteamUGC.CreateItem(RustSDK.sdkGame, EWorkshopFileType.k_EWorkshopFileTypeMicrotransaction);
            }
            catch (Exception exc)
            { 
                Console.WriteLine("Ошибка инициализации Steam (не запущен?) или нет нужной DLL:\n" + exc.Message);
                return new Tuple<int, string>(0, "Ошибка инициализации Steam");
            }


            Steam.Wait(pHandle);
            var result = Steam.Result<CreateItemResult_t>(pHandle);
            var downloadId = result.m_nPublishedFileId.m_PublishedFileId;

            Console.WriteLine(new Random().Next().ToString());
            
            var updateHandle = SteamUGC.StartItemUpdate(RustSDK.sdkGame, new PublishedFileId_t(downloadId));
            SteamUGC.SetItemTitle(updateHandle, new Random().Next().ToString());
            SteamUGC.SetItemDescription(updateHandle, "Данное изображение было загружено через сервис:\n" +
                "http://skin.hougan.space");
            SteamUGC.SetItemMetadata(updateHandle, "");
            SteamUGC.SetItemVisibility(updateHandle, ERemoteStoragePublishedFileVisibility.k_ERemoteStoragePublishedFileVisibilityPublic);

            var tags = new List<string>();
            tags.Add("version3");
            SteamUGC.SetItemTags(updateHandle, tags);

            var rootFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var dataFolder = $@"{rootFolder}/data";

            SteamUGC.SetItemContent(updateHandle, dataFolder);
            SteamUGC.SetItemPreview(updateHandle, $@"{rootFolder}/data/icon.png");

            var handle = SteamUGC.SubmitItemUpdate(updateHandle, "Initial release");
            while (!Steam.IsFinished(handle))
            {
                SteamAPI.RunCallbacks();

                ulong processed, total;
                var progress = 0.0f;
                var status = SteamUGC.GetItemUpdateProgress(updateHandle, out processed, out total);

                var str = string.Format("{0}", status);

                if (processed > 0 && total > 0)
                {
                    progress = ((float)processed) / ((float)total);
                    str = string.Format("{0} - {1}/{2}", status, processed, total);
                }

            }

            var rr = Steam.Result<SubmitItemUpdateResult_t>(handle);
            if (rr.m_eResult != EResult.k_EResultOK)
            {
                return new Tuple<int, string>(0, $"Ошибка: {rr.m_eResult}");
            }
            else
            {
                id = downloadId.ToString();
            }
            
            return new Tuple<int, string>(1, id);
        }
    }
}